﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[SerializeField]
public enum CubeType
{
    Ground,
    NeedItem
}
public class Cube : MonoBehaviour
{
    public CubeType CubeType;

    [SerializeField] private Item Item;
    [SerializeField] [Range(0f, 5f)] private float CubeDetectionRadius;

    List<Cube> _neighbours;
    bool _isAvailable = true;
    private void Awake()
    {
        GetNeighbours();
        InitializeItem();
    }
    void GetNeighbours()
    {
        _neighbours = new List<Cube>();
        var position = transform.position;
        var r = new Ray(position, Vector3.right * CubeDetectionRadius);
        if (Physics.Raycast(r, out RaycastHit hitRight, CubeDetectionRadius, (1 << 8)))
        {
            Cube c = hitRight.collider.gameObject.GetComponent<Cube>();
            _neighbours.Add(c);
        }
        r = new Ray(position, Vector3.left * CubeDetectionRadius);
        if (Physics.Raycast(r, out RaycastHit hitLeft, CubeDetectionRadius, (1 << 8)))
        {
            Cube c = hitLeft.collider.gameObject.GetComponent<Cube>();
            _neighbours.Add(c);
        }
        r = new Ray(position, Vector3.forward * CubeDetectionRadius);
        if (Physics.Raycast(r, out RaycastHit hitForward, CubeDetectionRadius, (1 << 8)))
        {
            Cube c = hitForward.collider.gameObject.GetComponent<Cube>();
            _neighbours.Add(c);
        }
        r = new Ray(position, Vector3.back * CubeDetectionRadius);
        if (Physics.Raycast(r, out RaycastHit hitBack, CubeDetectionRadius, (1 << 8)))
        {
            Cube c = hitBack.collider.gameObject.GetComponent<Cube>();
            _neighbours.Add(c);
        }
    }
    internal bool NextCube(Vector3 dir, out Cube cube)
    {
        cube = null;
        if (_neighbours.Count > 0)
        {
            foreach (Cube c in _neighbours)
            {
                var _d = (c.transform.position - transform.position).normalized;
                if (Vector3.Dot(dir, _d) == 1)
                {
                    cube = c;
                    return true;
                }
            }
        }
        return false;
    }
    internal void InitializeItem()
    {
        if (Item)
        {
            Item.Set2Cube(this);
        }
    }
    internal bool CanGo(out Item item)
    {
        item = null;
        if (CanDropItem())
        {
            return false;
        }
        else if (Item && !CubeType.Equals(CubeType.NeedItem))
        {
            item = Item;
            return false;
        }
        else if (!_isAvailable)
        {
            return false;
        }
        return true;
    }
    internal bool CanDropItem()
    {
        if (_isAvailable && !Item && CubeType.Equals(CubeType.NeedItem))
            return true;
        return false;
    }
    internal void ChangeAvailability(bool availability)
    {
        _isAvailable = availability;
    }
    internal void RemoveItem()
    {
        Item = null;
    }
    internal void AddItem(Item item)
    {
        Item = item;
        item.Set2Cube(this, false);
    }
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.magenta;
        var position = transform.position;
        Gizmos.DrawRay(position, Vector3.right * CubeDetectionRadius);
        Gizmos.DrawRay(position, Vector3.left * CubeDetectionRadius);
        Gizmos.DrawRay(position, Vector3.forward * CubeDetectionRadius);
        Gizmos.DrawRay(position, Vector3.back * CubeDetectionRadius);
    }
}
