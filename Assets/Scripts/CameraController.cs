﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public static CameraController Instance { get; private set; }

    [SerializeField] bool canMultipleObjectFollow;
    [SerializeField] [Range(.5f, 5f)] float CameraDistance = .5f;
    [SerializeField] [Range(.5f, 5f)] float CameraSpeed = 1f;

    private Camera _camera;
    private Transform _followingObject;
    private Transform[] _followingObjects;
    private Vector3 _initiallyCameraDir;
    private bool _isGameCompleted = false;
    float distanceOfObjects { get => Vector3.Distance(_followingObjects[0].position, _followingObjects[1].position); }

    private void OnEnable()
    {
        GameLogic.onComplete += GameLogic_onComplete;
        GameLogic.onStart += GameLogic_onStart;
    }
    private void OnDisable()
    {
        GameLogic.onComplete -= GameLogic_onComplete;
        GameLogic.onStart -= GameLogic_onStart;
    }

    private void Awake()
    {
        Instance = this;
        _camera = GetComponent<Camera>();

        _initiallyCameraDir = _camera.transform.forward;
    }
    private void Update()
    {
        if (!_followingObject) return;

        Vector3 newPos = default;
        if (!canMultipleObjectFollow)
            newPos = _followingObject.transform.position;
        else
            newPos = MedianOfFollowingObjects();
        newPos += -_initiallyCameraDir * Mathf.Lerp(CameraDistance / 1.5f, CameraDistance, Mathf.InverseLerp(.5f, 3f, distanceOfObjects));
        var toNewPosDir = newPos - _camera.transform.position;
        _camera.transform.position += toNewPosDir * CameraSpeed * Time.deltaTime;
    }
    internal void SetFollowingObject(Transform obj)
    {
        _followingObject = obj;
    }
    internal void SetMultipleFollowingObject(Transform[] objs)
    {
        _followingObjects = objs;
    }
    Vector3 MedianOfFollowingObjects()
    {
        Vector3 tempPos = _followingObjects[0].position;
        var dir = (_followingObjects[1].position - _followingObjects[0].position) / 2;
        return tempPos + dir;
    }
    public Vector3 GetCameraPos()
    {
        return _camera.transform.position;
    }
    private void GameLogic_onStart()
    {
        _isGameCompleted = false;
        _camera.transform.rotation = Quaternion.LookRotation(_initiallyCameraDir);
    }
    private void GameLogic_onComplete()
    {
        _isGameCompleted = true;
    }
}
