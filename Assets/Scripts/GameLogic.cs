﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameLogic : MonoBehaviour
{
    public static GameLogic Instance { get; private set; }

    [SerializeField] private Character[] Characters;
    [SerializeField] private Character Premses;

    private Character _activeCharacter;
    private bool _completed;
    private bool _gameFlowCompleted;

    public static event System.Action onComplete;
    public static event System.Action onGameFlowComplete;
    public static event System.Action onStart;
    private void OnEnable()
    {
        InputController.OnMouseMoved += InputController_OnMouseMoved;
        InputController.OnMouseUp += InputController_OnMouseUp;
        InputController.OnMouseDown += InputController_OnMouseDown;
        GameScreenUI.onChangeButtonClick += ChangeCharacter;
    }
    private void OnDisable()
    {
        InputController.OnMouseMoved -= InputController_OnMouseMoved;
        InputController.OnMouseUp -= InputController_OnMouseUp;
        InputController.OnMouseDown -= InputController_OnMouseDown;
        GameScreenUI.onChangeButtonClick -= ChangeCharacter;
    }
    private void Awake()
    {
        Instance = this;
    }
    private void Start()
    {
        Transform[] tempArr = new Transform[] { Characters[0].transform, Characters[1].transform };
        CameraController.Instance.SetMultipleFollowingObject(tempArr);
        _completed = false;

        onStart?.Invoke();

    }
    private void Update()
    {
        if (_completed) return;

        CheckPositionsAndComplete();

        if (Input.GetKeyDown(KeyCode.R))
            ChangeCharacter();
    }
    private void InputController_OnMouseMoved(Vector2 dir)
    {
        if (_completed) return;

        var limit = .01f;
        var calcDir = new Vector3(
            Mathf.Abs(dir.x) > Mathf.Abs(dir.y) + limit ? dir.x : 0f,
            0f,
            Mathf.Abs(dir.y) > Mathf.Abs(dir.x) + limit ? dir.y : 0f).normalized;
        if (_activeCharacter)
        {
            _activeCharacter.ControlNextCube(calcDir);
        }
    }
    private void InputController_OnMouseDown()
    {
        if (_gameFlowCompleted) return;

        Character c = ChoosePrince();
        if (c)
        {
            _gameFlowCompleted = true;
            switch (c.CharacterType)
            {
                case CharacterType.Crafter:
                    {
                        Characters[1].ApproachTo(c, Premses);
                    }
                    break;
                case CharacterType.Transporter:
                    {
                        Characters[0].ApproachTo(c, Premses);
                    }
                    break;
                case CharacterType.Premses:
                    {
                        int rand = Random.Range(0, 2);
                        if (rand == 0)
                            Characters[0].ApproachTo(c, Characters[1]);
                        else
                            Characters[1].ApproachTo(c, Characters[0]);
                    }
                    break;
            }
            onGameFlowComplete?.Invoke();
            StartCoroutine(EndRoutine());
        }
    }
    Character ChoosePrince()
    {
        if (_completed)
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out RaycastHit hit, 100f, (1 << 9)))
            {
                return hit.collider.gameObject.GetComponent<Character>();
            }
        }
        return null;
    }
    private void InputController_OnMouseUp()
    {
        _activeCharacter.Rotate2(CameraController.Instance.GetCameraPos());
    }
    public void SetActiveCharacter(Character character)
    {
        _activeCharacter = character;
        CameraController.Instance.SetFollowingObject(_activeCharacter.transform);
    }
    void ChangeCharacter()
    {
        if (Characters.Length > 0)
        {
            foreach (Character c in Characters)
            {
                if (c != _activeCharacter)
                {
                    SetActiveCharacter(c);
                    break;
                }
            }
        }
    }
    void RotateAllCharacters()
    {
        if (Characters.Length > 0 && Premses)
        {
            foreach (Character c in Characters)
            {
                c.Rotate2(CameraController.Instance.GetCameraPos());
            }
            Premses.Rotate2(CameraController.Instance.GetCameraPos());
        }
    }
    void ActivateCharacterTaunts()
    {
        if (Characters.Length > 0 && Premses)
        {
            foreach (Character c in Characters)
            {
                c.ActivateTauntAnim();
            }
            Premses.ActivateTauntAnim();
        }
    }
    void CheckPositionsAndComplete()
    {
        if (Characters.Length > 0 && Premses)
        {
            foreach (Character c in Characters)
            {
                if (Vector3.Distance(c.transform.position, Premses.transform.position) > 1.25f) return;
            }
            _completed = true;
            RotateAllCharacters();
            ActivateCharacterTaunts();
            onComplete?.Invoke();
            Debug.Log("Completed");
        }
    }
    IEnumerator EndRoutine()
    {
        yield return new WaitForSeconds(8f);
        GameManager.Instance.IncreaseLevel();
    }
}
