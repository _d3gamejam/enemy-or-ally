﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : MonoBehaviour
{
    public bool IsActive { get; private set; }
    public bool IsMoving { get; private set; }

    [SerializeField] private GameObject PassiveModel;
    [SerializeField] private GameObject ActiveModel;
    [SerializeField] GameObject Particle;

    private Vector3 offset = new Vector3(0f, 0f, 0f);
    private Vector3 dropOffset = new Vector3(0f, -.25f, 0f);
    private WaitForEndOfFrame wfeof = new WaitForEndOfFrame();
    internal void Activate()
    {
        IsActive = true;
        SetModelStatus();
    }
    void SetModelStatus()
    {
        Particle.SetActive(IsActive);
        PassiveModel.SetActive(!IsActive);
        ActiveModel.SetActive(IsActive);
    }
    internal void Set2Cube(Cube cube, bool immediate = true)
    {
        DropItem();
        if (immediate)
        {
            transform.position = cube.transform.position + offset;
            SetModelStatus();
        }
        else
        {
            StartCoroutine(PositionRoutine(cube.transform.position + offset + dropOffset));
            SetModelStatus();
        }
    }
    internal void GetItem(Transform bag)
    {
        transform.parent = bag;
    }
    internal void DropItem()
    {
        transform.parent = null;
    }
    IEnumerator PositionRoutine(Vector3 newPos)
    {
        IsMoving = true;
        var speed = 5f;
        var dir = newPos - transform.position;
        var rotDir = dir;
        rotDir.y = 0f;
        dir.Normalize();
        while (Vector3.Distance(newPos, transform.position) >= .1)
        {
            transform.position += dir * speed * Time.deltaTime;
            transform.localRotation = Quaternion.Lerp(transform.localRotation, Quaternion.LookRotation(rotDir), Time.deltaTime * speed * 10f);
            yield return wfeof;
        }
        IsMoving = false;
    }
}
