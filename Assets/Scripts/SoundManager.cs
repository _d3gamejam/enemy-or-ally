﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public enum SoundTypes
{
    Wood,
    Carry,
    Hit,
    Kiss,
    Warn
}
public class SoundManager : MonoBehaviour
{
    public static SoundManager Instance { get; private set; }
    [SerializeField] private List<AudioClip> Sounds;
    private void Awake()
    {
        Instance = this;
        DontDestroyOnLoad(this);
        SceneManager.LoadScene(PlayerPrefs.GetInt("CurrentLevel", 1));
    }

    public AudioClip GetSound(SoundTypes soundType)
    {
        return Sounds[(int)soundType];
    }
}
