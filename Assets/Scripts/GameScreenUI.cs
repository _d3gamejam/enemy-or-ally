﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameScreenUI : MonoBehaviour
{
    [SerializeField] Button ChangeButton;
    public Image ButtonImage;
    public Sprite[] Icons;
    int iconValue = 0;

    public static event System.Action onChangeButtonClick;
    private void Awake()
    {
        ChangeButton.onClick.AddListener(Handle_ChangeButton);

        iconValue++;
        if (iconValue >= 2)
            iconValue = 0;
        ButtonImage.sprite = Icons[iconValue];
    }
    void Handle_ChangeButton()
    {
        iconValue++;
        if (iconValue >= 2)
            iconValue = 0;
        ButtonImage.sprite = Icons[iconValue];
        onChangeButtonClick?.Invoke();
    }
}
