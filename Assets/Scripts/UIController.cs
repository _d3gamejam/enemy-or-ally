﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIController : MonoBehaviour
{
    public GameObject GameScreen;
    public GameObject ResultScreen;

    private void OnEnable()
    {
        GameLogic.onComplete += GameLogic_onComplete;
        GameLogic.onGameFlowComplete += GameLogic_onGameFlowComplete;
    }
    private void OnDisable()
    {
        GameLogic.onComplete -= GameLogic_onComplete;
        GameLogic.onGameFlowComplete -= GameLogic_onGameFlowComplete;
    }

    private void GameLogic_onGameFlowComplete()
    {
        GameScreen.SetActive(false);
        ResultScreen.SetActive(false);
    }

    private void GameLogic_onComplete()
    {
        GameScreen.SetActive(false);
        ResultScreen.SetActive(true);
    }
}
