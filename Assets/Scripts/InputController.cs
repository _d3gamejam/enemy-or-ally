﻿using UnityEngine;

public class InputController : MonoBehaviour
{
    [SerializeField] private Camera InputCamera;
    private bool _centerTaken;
    private Vector2 _initialMousePos;

    public static event System.Action<Vector2> OnMouseMoved;
    public static event System.Action OnMouseUp;
    public static event System.Action OnMouseDown;

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            OnMouseDown?.Invoke();
        }
        if (Input.GetMouseButton(0))
        {
            Vector2 mousePos = Input.mousePosition;
            Vector2 realPos = InputCamera.ScreenToWorldPoint(mousePos);
            if (!_centerTaken)
            {
                _initialMousePos = realPos;
                _centerTaken = true;
            }

            Vector2 direction = realPos - _initialMousePos;

            if (direction.magnitude != 0)
            {
                var speed = 20f;
                var deltaSpeed = 10f;
                var mouseDelta =
                    direction
                    * deltaSpeed
                    * Time.unscaledDeltaTime;
                OnMouseMoved?.Invoke(mouseDelta);
                _initialMousePos += direction.normalized * direction.magnitude * (speed / 1.5f) * Time.deltaTime;
            }

        }
        if (Input.GetMouseButtonUp(0))
        {
            OnMouseUp?.Invoke();
            _centerTaken = false;
        }
    }
}
