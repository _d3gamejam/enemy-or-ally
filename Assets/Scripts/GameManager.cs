﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance { get; private set; }
    int LevelCount = 2;

    int CurrentLevel
    {
        get => PlayerPrefs.GetInt("CurrentLevel", 1);
        set
        {
            if (value > LevelCount)
                PlayerPrefs.SetInt("CurrentLevel", 1);
            else
                PlayerPrefs.SetInt("CurrentLevel", value);
        }
    }
    private void Awake()
    {
        Instance = this;
        if (!SoundManager.Instance)
            SceneManager.LoadScene(0, LoadSceneMode.Additive);
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.L))
            IncreaseLevel();
    }
    public void IncreaseLevel()
    {
        CurrentLevel++;
        SceneManager.LoadScene(CurrentLevel);
    }
}
