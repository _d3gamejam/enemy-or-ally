using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[SerializeField]
public enum CharacterType
{
    None,
    Crafter,
    Transporter,
    Premses
}
public class Character : MonoBehaviour
{
    enum AnimationState
    {
        Idle,
        Walk,
        Crop,
        Hit,
        Shy,
        Kiss,
        Taunt1,
        Taunt2,
        Death1,
        Death2,
        Death3
    }
    public bool InitiallyActive;
    public CharacterType CharacterType;

    [SerializeField] private Cube CurrentCube;
    [SerializeField] private Transform Backpack;
    [SerializeField] [Range(.5f, 5f)] private float Speed = .5f;
    [SerializeField] private Animator Animator;
    [SerializeField] private GameObject DeathParticles;
    [SerializeField] private GameObject LoveParticles;
    [SerializeField] private AudioSource AudioSource;

    private Vector3 offset = new Vector3(0f, 0f, 0f);
    private WaitForEndOfFrame wfeof = new WaitForEndOfFrame();
    private bool _isMoving = false;
    private bool _isRotating = false;
    private bool _notInterruptableAnimationPlaying = false;
    private bool _tauntAnimationPlaying = false;
    private bool _deathAnimationPlaying = false;
    private bool _noWaitAnim = false;
    private Item _item;
    Queue<Vector3> rotateQueue = new Queue<Vector3>();
    private AnimationState animationState;
    private AnimationState _animationState
    {
        get => animationState;
        set
        {
            if (!value.Equals(_animationState))
            {
                if (value.Equals(AnimationState.Idle))
                    Animator.CrossFade(value.ToString(), .1f, 0, 0);
                else
                    Animator.Play(value.ToString(), 0, 0);

            }

            animationState = value;
        }
    }
    private void Awake()
    {
        transform.position = CurrentCube.transform.position + offset;
        transform.rotation = Quaternion.LookRotation(Vector3.back);
        CurrentCube.ChangeAvailability(false);
        AudioSource = GetComponent<AudioSource>();
    }
    private void Start()
    {
        if (InitiallyActive)
            GameLogic.Instance.SetActiveCharacter(this);
        _animationState = AnimationState.Idle;
    }
    internal void ControlNextCube(Vector3 calcDir)
    {
        if (CurrentCube.NextCube(calcDir, out Cube cube))
        {
            CanMove(cube);
        }
    }
    void CanMove(Cube cube)
    {
        if (!cube.CanGo(out Item item) && !_isMoving)
        {
            if (item)
            {
                switch (CharacterType)
                {
                    case CharacterType.Crafter:
                        {
                            if (!_notInterruptableAnimationPlaying && !item.IsActive)
                            {
                                StartCoroutine(CropAnimation(item));
                            }
                            //else
                            //{
                            //    AudioSource.clip = SoundManager.Instance.GetSound(SoundTypes.Warn);
                            //    AudioSource.Play();
                            //}
                        }
                        break;
                    case CharacterType.Transporter:
                        {
                            if (item.IsActive)
                            {
                                _item = item;
                                cube.RemoveItem();
                                item.GetItem(Backpack);
                                AudioSource.clip = SoundManager.Instance.GetSound(SoundTypes.Carry);
                                AudioSource.Play();
                                StartCoroutine(Item2HandRoutine());
                            }
                            //else
                            //{
                            //    AudioSource.clip = SoundManager.Instance.GetSound(SoundTypes.Warn);
                            //    AudioSource.Play();
                            //}
                        }
                        break;
                }
            }
            else if (cube.CanDropItem())
            {
                if (_item)
                {
                    DropItem(cube);
                }
            }
        }
        else
        {
            if (!_isMoving && !_notInterruptableAnimationPlaying)
            {
                CurrentCube.ChangeAvailability(true);
                CurrentCube = cube;
                CurrentCube.ChangeAvailability(false);
                Move();
            }
        }
    }
    void Move()
    {
        StartCoroutine(PositionRoutine(CurrentCube.transform.position + offset));
    }
    void DropItem(Cube cube)
    {
        AudioSource.clip = SoundManager.Instance.GetSound(SoundTypes.Carry);
        AudioSource.Play();
        cube.AddItem(_item);
        _item = null;
    }
    IEnumerator CropAnimation(Item item)
    {
        Rotate2(item.transform.position);
        _notInterruptableAnimationPlaying = true;
        _animationState = AnimationState.Crop;
        yield return wfeof;
        float duration = Animator.GetCurrentAnimatorStateInfo(0).length;
        yield return new WaitForSeconds(duration / 2f);
        AudioSource.clip = SoundManager.Instance.GetSound(SoundTypes.Wood);
        AudioSource.Play();
        if (!item.IsActive)
            item.Activate();
        yield return new WaitForSeconds(duration / 2f);
        _notInterruptableAnimationPlaying = false;
        _animationState = AnimationState.Idle;
    }
    IEnumerator PositionRoutine(Vector3 newPos, System.Action func = null)
    {
        _isMoving = true;
        _animationState = AnimationState.Walk;
        var dir = newPos - transform.position;
        dir.Normalize();
        while (Vector3.Distance(newPos, transform.position) >= .05f)
        {
            transform.position += dir * Speed * Time.deltaTime;
            transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(dir), Time.deltaTime * Speed * 10f);
            yield return wfeof;
        }
        _isMoving = false;

        yield return new WaitForSeconds(.1f);
        if (!_isMoving && !_notInterruptableAnimationPlaying && !_tauntAnimationPlaying && !_deathAnimationPlaying)
            _animationState = AnimationState.Idle;
        func?.Invoke();
    }
    IEnumerator Item2HandRoutine()
    {
        var speed = 7f;
        var endOffset = new Vector3(0, .25f, -.2f);
        var endPos = endOffset;

        //var dir = endPos - _item.transform.localPosition;
        //dir.Normalize();
        while (Vector3.Distance(endPos, _item.transform.localPosition) >= .01f)
        {
            _item.transform.localPosition = Vector3.Lerp(_item.transform.localPosition, endPos, speed * Time.deltaTime);
            _item.transform.rotation = Quaternion.Lerp(_item.transform.rotation, Quaternion.LookRotation(Backpack.up), Time.deltaTime * speed * 10f);
            yield return wfeof;
        }
    }
    internal void Rotate2(Vector3 pos)
    {
        rotateQueue.Enqueue(pos);
        if (!_isRotating)
            StartCoroutine(RotateTo());
    }
    IEnumerator RotateTo()
    {
        _isRotating = true;
        while (rotateQueue.Count > 0)
        {
            yield return new WaitWhile(() => (_isMoving || _notInterruptableAnimationPlaying) && !_noWaitAnim);
            var dir = rotateQueue.Dequeue() - transform.position;
            dir.y = 0f;
            dir.Normalize();
            float timer = 0;
            while (timer < .25f)
            {
                timer += Time.deltaTime;
                transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(dir), Time.deltaTime * Speed * 10f);
                yield return wfeof;
            }
        }
        _isRotating = false;
    }
    internal void ApproachTo(Character c, Character nextCharacter)
    {
        _tauntAnimationPlaying = false;
        var targetDir = c.transform.position - transform.position;
        var newTargetPos = transform.position + targetDir * .5f;

        var loverDir = this.transform.position - nextCharacter.transform.position;
        var newLoverPos = nextCharacter.transform.position + loverDir * .65f;

        StartCoroutine(
            PositionRoutine(newTargetPos,
            () => StartCoroutine(
                HitAnimation(c, () =>
                {
                    nextCharacter._tauntAnimationPlaying = false;
                    StartCoroutine(nextCharacter.PositionRoutine(newLoverPos,
                    () =>
                    {
                        StartCoroutine(nextCharacter.KissAnimation(this));
                        StartCoroutine(KissAnimation(nextCharacter));
                    }));
                }))));
    }
    IEnumerator HitAnimation(Character c, System.Action func = null)
    {
        yield return wfeof;
        _noWaitAnim = true;
        Rotate2(c.transform.position);
        _notInterruptableAnimationPlaying = true;
        switch (CharacterType)
        {
            case CharacterType.Crafter:
                {
                    _animationState = AnimationState.Crop;
                }
                break;
            case CharacterType.Transporter:
                {
                    _animationState = AnimationState.Hit;
                }
                break;
        }
        yield return wfeof;
        float duration = Animator.GetCurrentAnimatorStateInfo(0).length;
        yield return new WaitForSeconds(duration / 2f);
        c.ActivateDeath();
        yield return new WaitForSeconds(duration / 2f);
        c.DeathParticles.SetActive(true);
        _noWaitAnim = false;
        _notInterruptableAnimationPlaying = false;
        //_animationState = AnimationState.Idle;
        func?.Invoke();
    }
    IEnumerator KissAnimation(Character c)
    {
        Rotate2(c.transform.position);
        _notInterruptableAnimationPlaying = true;
        _animationState = AnimationState.Kiss;
        LoveParticles.SetActive(true);
        AudioSource.clip = SoundManager.Instance.GetSound(SoundTypes.Kiss);
        AudioSource.Play();
        yield return wfeof;
        float duration = Animator.GetCurrentAnimatorStateInfo(0).length;
        yield return new WaitForSeconds(duration / 2f);
        AudioSource.Play();
        yield return new WaitForSeconds(duration / 2f);
        AudioSource.Play();

        _notInterruptableAnimationPlaying = false;
    }
    internal void ActivateTauntAnim()
    {
        _tauntAnimationPlaying = true;
        switch (CharacterType)
        {
            case CharacterType.Crafter:
                {
                    _animationState = AnimationState.Taunt1;
                }
                break;
            case CharacterType.Transporter:
                {
                    _animationState = AnimationState.Taunt2;
                }
                break;
            case CharacterType.Premses:
                {
                    _animationState = AnimationState.Shy;
                }
                break;
        }
    }
    internal void ActivateDeath()
    {
        _deathAnimationPlaying = true;
        _tauntAnimationPlaying = false;
        switch (CharacterType)
        {
            case CharacterType.Crafter:
                {
                    _animationState = AnimationState.Death1;
                }
                break;
            case CharacterType.Transporter:
                {
                    _animationState = AnimationState.Death1;
                }
                break;
            case CharacterType.Premses:
                {
                    _animationState = AnimationState.Death1;
                }
                break;
        }
        AudioSource.clip = SoundManager.Instance.GetSound(SoundTypes.Hit);
        AudioSource.Play();
    }
}
